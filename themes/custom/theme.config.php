<?php
/*
1. Add "Map" in addition to list and grid in sls/dev4/local/config/vufind/searches.ini





*/


return array(
    'extends' => 'finna2',
    'css' => array(
        'custom.css',
        'settings.css',
        'L.Control.Sidebar.css',
        'MarkerCluster.css',
        'MarkerCluster.Default.css'

    ),
    'js' => array(
        'L.Control.Sidebar.js',
        'leaflet.markercluster.js'
    )
);

