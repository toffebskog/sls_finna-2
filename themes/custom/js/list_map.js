/*global VuFind, finna */
finna.resultsOnMap = (function slsResultsOnMap() {

    // polyfill for urlSearchParams
    (function (w) {
    
        w.URLSearchParams = w.URLSearchParams || function (searchString) {
            var self = this;
            self.searchString = searchString;
            self.get = function (name) {
                var results = new RegExp('[\?&]' + name + '=([^&#]*)').exec(self.searchString);
                if (results == null) {
                    return null;
                }
                else {
                    return decodeURI(results[1]) || 0;
                }
            };
        }
    
    })(window)

    function loadMany() {
        var urlParams = new URLSearchParams(window.location.search);
        var limit = urlParams.get('limit');
        var l = window.location.toString();
        if (!l.includes("limit")) {
            $('body').hide();
            window.location = l + "&limit=1000";
        } else if (limit !== '1000') {
            $('body').hide();
           window.location = l.replace('limit=' + limit, 'limit=1000');
        }
    }
    
    var createLocation = function(name) {
        return {
            "name" : name,
            "children": [],
            "ids": [],
            "markers": [],
            "bounds": new L.LatLngBounds()
        };
    }
    
    var findByName = function (arr, name) {
        for (var item of arr) {
            if (item.name === name)
                return item;
        }
    }
    
    var location_hierarchy = [];
    function addLocation(location_levels, marker) {
        var loc = location_hierarchy;
        var prev_name = "";
        for (var i in location_levels) {
            var name = location_levels[i];
            var next_loc = findByName(loc, name);
            if (!next_loc) {
                next_loc = createLocation(name);
                loc.push(next_loc);
            }
            next_loc["ids"].push(marker.options.id);
            next_loc["markers"].push(marker);
            next_loc["bounds"].extend(marker.getLatLng());
            var center = next_loc["bounds"].getCenter();
            next_loc["lat"] = center.lat;
            next_loc["lng"] = center.lng;
            loc = next_loc["children"];
        }
    }
    
    
    

    var towns = [];
    var town_items = [];
    var town_bounds = [];
    function addTown(town) {
        if (!towns.includes(town)) {
            towns.push(town);
            town_items[town] = [];
            town_bounds[town] =  new L.LatLngBounds();
            
        }
    }

    var regions = [];
    var region_items = [];
    var region_towns = [];
    var region_bounds = [];
    function addRegion(region) {
        if (!regions.includes(region)) {
            regions.push(region);
            region_items[region] = [];
            region_towns[region] = [];
            region_bounds[region] =  new L.LatLngBounds();
        }
    }

    var town_region = [];
    function addTownToRegion(town, region) {
        addRegion(region);
        addTown(town);
        town_region[town] = region;
        if (!region_towns[region].includes(town)) {
            region_towns[region].push(town);
        }
    }

    function associateItemWithTown(id, town, marker) {
        addTown(town);
        town_items[town].push(id);
        town_bounds[town].extend(marker.getLatLng());
    }

    function associateItemWithRegion(id, region, marker) {
        addRegion(region);
        region_items[region].push(id);
        region_bounds[region].extend(marker.getLatLng());
    }
    
    
    function getGeoData(id) {
        var geodata = {};
        var $d = $("#" + id);
        geodata.id = $d.attr("id");
        geodata.lat = $d.data("lat");
        geodata.long = $d.data("long");
        geodata.town = $d.data("town");
        geodata.region = $d.data("region");
        geodata.location = $d.data("locationlevels").split("|");
        return geodata;
    }

    function zoomToLocation(location) {
        prevent_zoom_visibility_toggle = true;
        var zoom = map.getBoundsZoom(location["bounds"]);
        zoom = zoom < 10 ? zoom: 10;
        map.flyTo(location["bounds"].getCenter(), zoom);
        $('.list-map--item').hide();
        clearMarkers();
        for (var id of location["ids"]) {
          $("#" + id).show();
          marker_lookup_table[id].addTo(markers);
        }
        map.once('moveend', function() {
           prevent_zoom_visibility_toggle = false;
        });
    }

    function thisIsALeafCluster(a) {
        
        var leaf = true;
        var all_markers = a.layer.getAllChildMarkers();
        var first_marker_html = all_markers[0].options.icon.options.html;
        var all_markers_html = "";
        for (m of all_markers) {
            all_markers_html += m.options.icon.options.html;
            //m.options.icon.options.className = "animating map-marker";
        }
        var regex = new RegExp(first_marker_html, "g");
        all_markers_html = all_markers_html.replace(regex, "");
        if (all_markers_html.length > 0) {
            return false;
        } else {
            return true;
        }
    }

    var open_item = null;
    // kolla https://github.com/ghybs/Leaflet.MarkerCluster.Freezable
    // kolla in https://github.com/Leaflet/Leaflet.markercluster#customising-the-clustered-markers
    var markers = markers = L.markerClusterGroup({
        showCoverageOnHover: false,
        spiderfyOnMaxZoom: true,
        zoomToBoundsOnClick: true,
        iconCreateFunction: function (cluster) {
            var markers = cluster.getAllChildMarkers();
            var location_list = [];
            var cluster_text = "";
            for (m of markers) {
                var geodata = getGeoData(m.options.id);
                for (var i in geodata.location) {
                    if (!location_list[i]) {
                        location_list[i] = [];
                    }
                    if (!location_list[i].includes(geodata.location[i])) {
                        location_list[i].push(geodata.location[i]);
                    }
                }
            }
            cluster_text = location_list[0][0];
            //var zoom = map.getZoom();
            
            var l = 1;
            var level = 1;
            var only_one = true;
            for (var i in location_list) {
                if (location_list[i].length !== 1) {
                    only_one = false;
                    break;
                }
            }
            for (var i in location_list) {
                if (location_list[i].length > 6) {
                    break;
                } else if (i < 3 || only_one) {
                    level = i;
                    l = location_list[i].length;
                    cluster_text = location_list[i][0];
                    var cluster_text = location_list[i].join('</li><li>');
                    if (l > 1 || l > 5) {
                        cluster_text = location_list[i].join('</li><li>');
                    } 
                    cluster_text = '<ul class="regionnames"><li>' + cluster_text +'</li></ul>';                    
                }
            }
            //var cluster_text = cluster_towns.join('</li><li>');
            //if (cluster_regions.length > 1 || cluster_towns.length > 5) {
            //    cluster_text = cluster_regions.join('</li><li>');
            //} 
            //cluster_text = '<ul><li>' + cluster_text+ '</li></ul>';
            
            var html = '<div class="circle contains-'+l+' level-'+level+'">' + markers.length  + cluster_text + '</div>';
            return L.divIcon({ html: html, className: 'ortCluster', iconSize: L.point(32, 32)});
        }
    });
    markers.on('clusterclick', function (a) {
        if (thisIsALeafCluster(a)) {
            for (m of a.layer.getAllChildMarkers()) {
                m.options.icon.options.className = "animating map-marker";
            }
        }
    });
    
    markers.on('animationend', function (a) {
        if (a.layer) {
            for (m of a.layer.getAllChildMarkers()) {
                m.options.icon.options.className = "map-marker";
            }
        }
    });    
    
    
    markers.on('spiderfied', function(data) {
        for (m of data.markers) {
            m._icon.classList.add('spiderified');
        }
    });
    markers.on('unspiderfied', function(data) {
        for (m of data.markers) {
            m._icon.classList.remove('spiderified');
        }
    });

    var clearMarkers = function() {
        for (id in marker_lookup_table) {
            markers.removeLayer(marker_lookup_table[id]);
        }
    }
    var restoreMarkers = function() {
        for (id in marker_lookup_table) {
            marker_lookup_table[id].addTo(markers);
        }
    }
    var marker_lookup_table = {};
    function addMarkers(map) {

        $(".list-map--item").each(function() {
            var id = $(this).attr("id");
            var gd = getGeoData(id);
            var label = gd.location[gd.location.length-1];
            
            var iconlabel = $(this).find(".label.label-info.iconlabel").attr('class').replace('label label-info', '');
            var icon = L.divIcon({
             className: 'map-marker',
             iconSize:null,
             html:'<div class="circle contains-1 level-'+(gd.location.length-1)+' icon"><span class="'+iconlabel+'"></span><ul class="regionnames"><li>' + label + '</li></ul></div>'
             
            });
            var marker = new L.marker([gd.lat, gd.long], {id: id, icon: icon, location: gd.location}).addTo(markers);
            addLocation(gd.location, marker);
            marker_lookup_table[id] = marker;
            marker.on('click', function(e) {
                showSidebarFor(id);
            });

            associateItemWithTown(id, gd.town, marker);
            associateItemWithRegion(id, gd.region, marker);
            addTownToRegion(gd.town, gd.region);

        });
        // special case when regions are added as Borg� Landskommun(Borg�)
        /*var remove_regions = [];
        for (let i in regions) {
            let r = regions[i];
            if (towns.includes(r)) {
                remove_regions.push(i);
                console.log("i have seen " + r + " as a town");
                for (var rt of region_towns[r]) {
                    console.log("what to do with: " + rt);
                    town_bounds[r].extend(region_bounds[r]);
                }
                regions
                
            }
        }
        for (let i of remove_regions) {
            //regions.splice(i,1);
        }*/
        map.addLayer(markers);
    }


    function closeSidebar(callback) {
        var gd = getGeoData(open_item);
        var rect = map.latLngToContainerPoint(L.latLng([gd.lat,gd.long]));

        animateClose(rect, callback);
        sidebar.hide();
        open_item = null;
        $(".list-map--item.active").removeClass("active");
        return false;

    }
    
    
    function showSidebarFor(id) {
        $(".list-map--item.active").removeClass("active");

        var gd = getGeoData(id);
        var rect = map.latLngToContainerPoint(L.latLng([gd.lat,gd.long]));

        
        if (open_item != id) {
            $("#map_popover .map_popover--content").html($("#" + id + " .sidebarcontent").clone().show());
            $("#map_popover .map_popover--content").append('<div id="transcription"></div>');
            $("#" + id).addClass("active");
            open_item = id;
            loadTranscriptionFor(id);
            animateOpen(rect);

            //sidebar.show();
        } else {
            animateClose(rect);
        }
    }
    
    function animateOpen(rect) {
        var ot = $("#mapid").offset().top;
        var ol = $("#mapid").offset().left;
        var top = rect.y + ot;
        var left = rect.x + ol;
        $("#map_popover .sidebarcontent").hide();
        $("#map_popover").css({top: top + "px", left: left + "px", width: 0, height: 0, opacity: 1});
        $("#map_popover").show();
        $("#map_popover").animate({top: ot + 20 + "px", left: -280 + ol + "px", width: "860px", height: "560px", opacity: 1},
            // then...
            function() {
                $("#map_popover .sidebarcontent").show();
            }
        );
        
    }

    function animateClose(rect, callback) {
        var ot = $("#mapid").offset().top;
        var ol = $("#mapid").offset().left;
        var top = rect.y + ot;
        var left = rect.x + ol;
        $("#map_popover .sidebarcontent").hide();

        $("#map_popover").css({top: ot + 20 + "px", left: -280 + ol + "px", width: "860px", height: "560px", opacity: 1});
        $("#map_popover").animate({top: top + "px", left: left + "px", width: 0, height: 0, opacity: 0.1}, function() {
            $("#map_popover").hide();
            if (callback) {
                callback();
            }
        });
    }
    



    function getPlaceSorter(place_list, attribute_name, compare_fn) {
        place_list = place_list || region_bounds;
        attribute_name = attribute_name || 'lat';
        compare_fn = (x, y) => {x < y};
        
        return function(a, b){
            var a_pos = parseFloat(place_list[a].getCenter()[attribute_name]);
            var b_pos = parseFloat(place_list[b].getCenter()[attribute_name]);
            if (compare_fn(a_pos,b_pos)) {
                return -1;
            } else {
                return 1;
            }
        }
    
    }
    var loc_index = [];
    function addSubMenu(locations, $menu, level, location_id) {
        var $locs = [];
        var $uls = [];
        var $as = [];
        for (var i in locations) {
            var loc = locations[i];
            var name = loc["name"];
            var l_id = location_id + '|'+ name;
            loc_index[l_id] = loc;

            $as[i] = $('<a class="toggler"></a>');
            $locs[i] = $('<li><a class="toggler" id="'+l_id+'">'+name+'</a></li>')
            if (loc.children.length) {
                $uls[i] = $('<ul class="accordion level-'+level+'"></ul>');
                if (name == "�sterbotten" || name == "Pohjanmaa") {
                    loc.children.sort(sorters.location.ns);
                } else {
                    loc.children.sort(sorters.location.ew);
                }
                addSubMenu(loc.children, $uls[i], level+1, l_id);
                $locs[i].append($uls[i]);
            }
            $menu.append($locs[i]);
        }
    }
    
    function createAccordion() {
        var $regions, $region, $towns, $town, $items, $item,
            region, town, item_id;
        $locations = $('<ul class="accordion level-1"></ul>');
        addSubMenu(location_hierarchy, $locations, 2, "locations");
        
 
        
        
        $('.sidebar.right .regionlist').html($locations);
        $('.accordion').hide();
        $('.accordion li > .toggler').click(function() {
            $('.accordion').hide();
            $(this).parents('.accordion').show();
            $(this).parent().find('.accordion').first().show();
            var region = $(this).text();
            zoomToLocation(loc_index[$(this).attr('id')]);
        });
        $('.accordion.level-1').show();
        $('.accordion.level-2').show();

    }

    var sorters = {
        location: {
            ns: function(a, b) { // north to south
                var a_pos = parseFloat(a.lat);
                var b_pos = parseFloat(b.lat);
                if (a_pos > b_pos)
                    return -1;
                else
                    return 1;
                return 0;
            }, 
            ew: function(a, b) { // east to west
                var a_pos = parseFloat(a.lng);
                var b_pos = parseFloat(b.lng);
                if (a_pos < b_pos)
                    return -1;
                else
                    return 1;
                return 0;
            }
        },
        region: {
            ns: function(a, b) { // north to south
                var a_pos = parseFloat(region_bounds[a].getCenter().lat);
                var b_pos = parseFloat(region_bounds[b].getCenter().lat);
                if (a_pos > b_pos)
                    return -1;
                else
                    return 1;
                return 0;
            }, 
            ew: function(a, b) { // north to south
                var a_pos = parseFloat(region_bounds[a].getCenter().lng);
                var b_pos = parseFloat(region_bounds[b].getCenter().lng);
                if (a_pos < b_pos)
                    return -1;
                else
                    return 1;
                return 0;
            }
        },
        town: {
            ew: function(a, b) { // east to west
                var a_pos = parseFloat(town_bounds[a].getCenter().lng);
                var b_pos = parseFloat(town_bounds[b].getCenter().lng);
                if (a_pos < b_pos) {
                    return -1;
                } else {
                    return 1;
                }
                return 0;
            },
            ns: function(a, b) { // north to south
                var a_pos = parseFloat(town_bounds[a].getCenter().lat);
                var b_pos = parseFloat(town_bounds[b].getCenter().lat);
                if (a_pos > b_pos) {
                    return -1;
                } else {
                    return 1;
                }
                return 0;
            }
        }
    }

    var prevent_zoom_visibility_toggle = false;
    var current_zoomlevel = null;

    var map;
    var sidebar;
    function init() {

	    map = L.map('mapid').setView([62.9241, 22.748], 6);
        L.Icon.Default.imagePath = '/dev2/themes/custom/images/map-markers/';

    	L.tileLayer('https://server.arcgisonline.com/ArcGIS/rest/services/World_Topo_Map/MapServer/tile/{z}/{y}/{x}', {
    	//L.tileLayer('https://server.arcgisonline.com/ArcGIS/rest/services/Canvas/World_Light_Gray_Base/MapServer/tile/{z}/{y}/{x}', {
    	//L.tileLayer('https://{s}.basemaps.cartocdn.com/light_nolabels/{z}/{x}/{y}{r}.png', {
    	//L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
    	//L.tileLayer('https://server.arcgisonline.com/ArcGIS/rest/services/World_Topo_Map/MapServer/tile/{z}/{y}/{x}', {
        }).addTo(map);

    	sidebar = L.control.sidebar('mapsidebar', {
          position: 'right',
          closeButton: true,
          autoPan: false
        });

        map.addControl(sidebar);
        current_zoomlevel = map.getZoom();


    
        function getFeaturesInView() {
          var features = [];
          var mb = map.getBounds();
          markers.eachLayer( function(layer) {
            if(layer instanceof L.Marker) {
              if(mb.contains(layer.getLatLng())) {
                features.push(layer);
              }
            }
          });
          return features;
        }
        
        function registerEventListeners() {
            $(".map_popover--close").click(function() {
                closeSidebar();
                return false;
            });
            
            $(".list-map--item").click(function() {
                var id = $(this).attr("id");
                if (open_item == id) {
                    closeSidebar();
                } if (open_item) {
                    closeSidebar(function() {showSidebarFor(id)});
                    
                } else {
                    showSidebarFor(id)
                }
            });
        
            map.on('moveend', showVisible);

            map.on('zoomend', function() {
                var zoom = map.getZoom();
                if (zoom < current_zoomlevel) {
                    $(".accordion.towns").hide();
                }
                showVisible();
                current_zoomlevel = zoom
            });
        }
        
        function inBounds(bounds, latLng) {
            return true;
        }
        
        function showVisible() {
            if (!open_item && !prevent_zoom_visibility_toggle) {
              restoreMarkers();
              var markers = getFeaturesInView();
              $('.list-map--item').hide();
              for (m of markers) {
                $("#" + m.options.id).show();
              }
            }
        }

        loadMany();

        addMarkers(map);
        createAccordion();
        registerEventListeners();
        $("#map_popover").hide();
        map.on('click', function(e){
            closeSidebar();
        });
    }
    
    var my = {
        init: init
    };

  return my;
})();